cdist-type__p0d_clean_ubuntu(7)
===============================

NAME
----
cdist-type__p0d_clean_ubuntu - Clean Ubuntu.


DESCRIPTION
-----------
Clean an *Ubuntu 22.04 Minimal Server Installation*.

Basically remove and disable Canonical snap_ and a few other things.

.. _snap: https://github.com/snapcore/

REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
None.


BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

    __p0d_clean_ubuntu


SEE ALSO
--------
:strong:`__p0d_debian_reset`\ (7) same but oriented toward Desktop installations.


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
